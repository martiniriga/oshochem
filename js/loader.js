***************VARIABLES******************************/
var siteUrl = 'https://oshoche.sharepoint.com/';
var documents;

/********************************************fetch Documents*******************************************/
function retrieveDocuments(folderName) {
    var clientContext = new SP.ClientContext(siteUrl + "docs/");
    var oList = clientContext.get_web().get_lists().getByTitle("Documents");

    var camlQuery = new SP.CamlQuery();
    camlQuery.set_viewXml("<View><Query/></View>");
    //var camlQuery = SP.CamlQuery.createAllItemsQuery();
    camlQuery.set_folderServerRelativeUrl(folderName + '/');
    documents = oList.getItems(camlQuery);

    clientContext.load(documents, 'Include(Id,DisplayName,ContentType,Editor,Modified,ServerUrl,File_x0020_Type,File_x0020_Size,BaseName)');

    clientContext.executeQueryAsync(Function.createDelegate(this, function(){onDocFetchSucceeded(folderName);}), Function.createDelegate(this, this.onDocFetchFailed));
}


function onDocFetchSucceeded(folderName) {

    var docInfo = '';
    var docEnumerator = documents.getEnumerator();
  
    while (docEnumerator.moveNext()) {
        var odoc = docEnumerator.get_current();
           	/********************Documents Loop*******************************/
	   	if(odoc.get_contentType().get_name()=="Document"){
	   	//if(odoc.get_item('ContentType').get_name()=="Document"){
	                name = odoc.get_displayName() + '.' + odoc.get_item('File_x0020_Type');
	                document_element +='<div class="doc_file doc">';
	            }
	     else{
	     	document_element +='<div class="doc_folder doc">';
	     	name = odoc.get_displayName();
	     }

	   	document_element +='<span class="doc_title">' + name + '</span>';
	   	document_element +='<span class="doc_modified">Modified By: ' + odoc.get_item('Editor').get_lookupValue() + '</span>';
	   	if(odoc.get_contentType().get_name()=="Document")
	   	//if(odoc.get_item('ContentType').get_name()=="Document")
	   		document_element +='<span class="doc_size">File Size: ' + bytesToSize(odoc.get_item('File_x0020_Size')) +' </span>';
	   		document_element +='<span class="doc_url">' + odoc.get_item('ServerUrl') +' </span>';
	   	document_element +='</div>';

	    /********************End Documents Loop****************************/
    }
	removeLoader();
    if(documents.get_count()>0){
    	if(!$('.populate_current_folder').length){
    		document_element += '</div>';
    		document_element += jpanel;
    		$('.populate-content').append(document_element);
    	}
    	else{
    		$('.document_items').empty();
    		$('.document_items').after(jpanel);
    		$('.document_items').append(document_element);
    	}

    }
    else
		$('.populate-content').append('<span class="no_files">Folder is Empty</span>');
}

function onDocFetchFailed(sender, args) {

    $('.populate-content').append('<span class="doc_error">Failed with Error: ' + args.get_message() + '\n' + args.get_stackTrace() + '</span>');
}
